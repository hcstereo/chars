package com.chars.vars.servlet;

import com.chars.vars.servlet.user.BaseHttpServlet;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Kevin on 12/11/15.
 * <p/>
 *
 * @author Kevin
 */
public abstract class BaseJsonServlet extends BaseHttpServlet {

    private HttpServletRequest request;

    private HttpServletResponse response;
    /**
     * Google gson
     */
    private Gson gson = new Gson();

    private PrintWriter printWriter;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        initVars(request,response);
        super.doGet(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        initVars(request,response);
        super.doPost(request, response);
    }

    /**
     * transfer Object to json strings.
     * @param object
     * @return
     */
    protected String toJson(Object object){
        return gson.toJson(object);
    }
    /**
     * print strings to client.
     * @param resStrings
     */
    protected void writeStrings(String resStrings){
        getPrintWriter().print(resStrings);
    }
    /**
     * transfer object to json strings,and print to client.
     * @param object
     */
    protected void writeObject(Object object){
        getPrintWriter().print(gson.toJson(object));
    }

    private void initVars(HttpServletRequest request,HttpServletResponse response){
        this.request = request;
        this.response = response;
        try {
            printWriter = response.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected PrintWriter getPrintWriter() {
        return printWriter;
    }
}
