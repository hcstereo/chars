package com.chars.vars.servlet;

import com.chars.vars.dao.UserDao;

/**
 * Created by Kevin on 12/11/15.
 * <p/>
 * This is a super class of user servlet,
 * define some rules about user's interface.
 * @author Kevin
 */
public abstract class UserServlet extends BaseJsonServlet{
    /**
     * This instance is used to operate user table
     */
    private UserDao userDao = new UserDao();

    protected UserDao getUserDao(){
        return userDao;
    }
}
