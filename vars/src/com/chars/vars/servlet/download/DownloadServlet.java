package com.chars.vars.servlet.download;

import com.chars.vars.model.DownloadFile;
import com.chars.vars.servlet.user.BaseHttpServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by Kevin on 12/13/15.
 * <p/>
 * provide a interface for client to download files.
 * @author Kevin
 */
@javax.servlet.annotation.WebServlet(name = "download",urlPatterns = "/download",
        initParams = {@javax.servlet.annotation.WebInitParam(name = "contentType",value = "application/octet-stream"),
                      @javax.servlet.annotation.WebInitParam(name = "fileRoot",value = "/Users/luffy/IdeaProjects/chars/vars/web/download/"),
        })
public class DownloadServlet extends BaseHttpServlet{

    private static final String TAG = DownloadServlet.class.getSimpleName();
    private Logger LOG = LogManager.getLogger(TAG);
    private String contentType;
    private String fileRoot;
    private static final int DEFAULT_WRITE_FILE_SIZE = 4096;

    @Override
    public void init() throws ServletException {
        super.init();
        fileRoot = getInitParameter("fileRoot");
        contentType = getInitParameter("contentType");
    }

    @Override
    protected void doSubGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    @Override
    protected void doSubPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filePath = request.getParameter(DownloadFile.DOWNLOAD_FILE_PATH);
        LOG.info("request for download file named "+filePath);
        DownloadFile downloadFile = new DownloadFile(fileRoot+filePath);
        if(downloadFile.exists()){
            LOG.info("file "+filePath+" exists start to download.");
            initResponse(response, downloadFile);
            putFile(response, downloadFile);
            LOG.info("file " + filePath + " exists finish to download.");
        }else{
            LOG.info("file "+filePath+" not exists. return.");
        }
    }
    private void initResponse(HttpServletResponse response,DownloadFile downloadFile){
        response.setContentType(contentType);
        response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
        Long length = downloadFile.length();
        LOG.info("file " + downloadFile.getAbsolutePath() + "'s length is " + length);
        response.setContentLength(length.intValue());
        response.addHeader("Content-Disposition", "attachment; filename=" + downloadFile.getName());
    }
    private void putFile(HttpServletResponse response,DownloadFile downloadFile) throws IOException {
        ServletOutputStream outputStream = response.getOutputStream();
        FileInputStream inputStream = new FileInputStream(downloadFile.getFile());
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        int size;
        byte[] b = new byte[DEFAULT_WRITE_FILE_SIZE];
        while ((size = bufferedInputStream.read(b)) != -1){
            LOG.info("write to output stream, and size is "+size+".");
            outputStream.write(b,0,size);
        }
        outputStream.flush();
        outputStream.close();
        bufferedInputStream.close();
    }
}
