package com.chars.vars.servlet.user;

import com.chars.vars.model.MUser;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luffy on 12/6/15.
 */
@javax.servlet.annotation.WebServlet(name = "ListUser",urlPatterns = "/list")
public class ListUser extends HttpServlet {

    private static final String TAG = ListUser.class.getSimpleName();

    static Logger Log = LogManager.getLogger(TAG);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Log.info("doGet() "+request.getRequestURL());
        Log.info(request.getRemoteAddr());
        PrintWriter writer = response.getWriter();
        Gson gson = new Gson();
        List<MUser> users = new ArrayList<>();
        for (int i = 0;i < 10; i++){
            MUser user = new MUser();
            user.setAge(i+10);
            user.setGender("男");
            user.setUserName("第" + i + "个用户");
            users.add(user);
        }
        String userJson = gson.toJson(users);
        writer.println(userJson);
        Log.info("writer.println() content=" + userJson);
    }
}
