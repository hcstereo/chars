package com.chars.vars.servlet.user;


import com.chars.vars.model.MUser;
import com.chars.vars.servlet.UserServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Kevin on 12/11/15.
 * <p/>
 * fetch user info interface.
 * @author Kevin
 */
@WebServlet(name = "FetchUserInfo",urlPatterns = "/user/fetchUInfo")
public class FetchUserInfo extends UserServlet {
    private static final String TAG = FetchUserInfo.class.getSimpleName();
    private static Logger LOG = LogManager.getLogger(TAG);

    @Override
    protected void doSubGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter(MUser.USER_KEY_USERNAME);
        LOG.debug("user "+username+" is trying to fetch user info ----START.");
        MUser user = getUserDao().fetchUserInfo(username);
        writeObject(user);
        LOG.debug("user "+username+" is trying to fetch user info ----END.");
    }

    @Override
    protected void doSubPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request,response);
    }


}
