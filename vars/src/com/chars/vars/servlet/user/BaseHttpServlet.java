package com.chars.vars.servlet.user;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Kevin on 12/13/15.
 * <p/>
 *
 * @author Kevin
 */
public abstract class BaseHttpServlet extends HttpServlet {
    /**
     * super class implements this method ,
     * do some common operations.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doSubPost(request, response);
    }

    /**
     * super class implements this method ,
     * do some common operations.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doSubGet(request, response);
    }

    /**
     * subclass implements this method,and
     * deal some operations from client by GET.
     * @param request
     * @param response
     */
    protected abstract void doSubGet(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException ;

    /**
     * subclass implements this method,and
     * deal some operations from client by POST.
     * @param request
     * @param response
     */
    protected abstract void doSubPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException ;

}
