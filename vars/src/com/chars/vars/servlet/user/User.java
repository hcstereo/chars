package com.chars.vars.servlet.user;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by luffy on 10/15/15.
 */
@javax.servlet.annotation.WebServlet(name = "User",urlPatterns = "/uu")
public class User extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        PrintWriter writer = response.getWriter();
        if(username.equals("Kevin") && password.equals("123123")){
            writer.print("login success");
        }else{
            writer.print("login failed");
        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("User Servlet init()");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("User Servlet destroy()");
    }
}
