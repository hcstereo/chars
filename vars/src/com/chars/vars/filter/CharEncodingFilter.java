package com.chars.vars.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by Kevin on 12/6/15.
 * <p/>
 *
 * @author Kevin
 */
@WebFilter(filterName = "CharEncodingFilter",urlPatterns = "/*")
public class CharEncodingFilter implements Filter {

    private static final String DEFAULT_CHARACTER_ENCODING = "UTF-8";

    private static final String TAG = CharEncodingFilter.class.getSimpleName();

    static Logger Log = LogManager.getLogger(TAG);

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        Log.entry();
        Log.debug("CharEncodingFilter doFilter() set set character encoding to " + DEFAULT_CHARACTER_ENCODING);
        req.setCharacterEncoding(DEFAULT_CHARACTER_ENCODING);
        resp.setCharacterEncoding(DEFAULT_CHARACTER_ENCODING);
        chain.doFilter(req, resp);
        Log.exit();
    }

    public void init(FilterConfig config) throws ServletException {
        Log.debug("CharEncodingFilter init()");
    }

}
