package com.chars.vars.system;

/**
 * Created by Kevin on 12/11/15.
 * <p/>
 *
 * @author Kevin
 */
public class APIDefine {
    /**
     * define some keys about user.
     */
    public interface IUser{
        String USER_KEY_USERNAME = "user_name";
        String USER_KEY_GENDER = "user_gender";
        String USER_KEY_AGE = "user_age";
    }

    public interface IDownload{
        String DOWNLOAD_FILE_PATH = "download_path";
    }
}
