package com.chars.vars.model;

import com.chars.vars.system.APIDefine;

import java.io.File;

/**
 * Created by Kevin on 12/13/15.
 * <p/>
 * A model to deal download request.
 * @author Kevin
 */
public class DownloadFile implements APIDefine.IDownload{
    private final File file;

    public DownloadFile(String fileName){
        file = new File(fileName);
    }

    public boolean exists(){
        return file.exists();
    }

    public long length(){
        return file.length();
    }

    public String getAbsolutePath(){
        return file.getAbsolutePath();
    }
    public String getName(){
        return file.getName();
    }
    public File getFile(){
        return this.file;
    }
}
