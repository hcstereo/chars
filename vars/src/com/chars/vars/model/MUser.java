package com.chars.vars.model;

import com.chars.vars.system.APIDefine;

/**
 * Created by luffy on 12/6/15.
 */
public class MUser implements APIDefine.IUser{
    private String userName;
    private String gender;
    private int age;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
