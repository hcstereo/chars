package com.chars.vars.dao.impl;

import com.chars.vars.model.MUser;

import java.util.List;

/**
 * Created by Kevin on 12/11/15.
 * <p/>
 * This interface defines some method about user,
 * such as fetch user info,login or change user info etc.
 * @author Kevin
 */
public interface IUserDao {
    MUser fetchUserInfo(String username);
}
