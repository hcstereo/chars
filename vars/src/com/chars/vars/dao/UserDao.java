package com.chars.vars.dao;

import com.chars.vars.dao.impl.IUserDao;
import com.chars.vars.model.MUser;

/**
 * Created by Kevin on 12/6/15.
 * <p/>
 * 
 * @author Kevin
 */
public class UserDao implements IUserDao {

    @Override
    public MUser fetchUserInfo(String username) {
        MUser user = new MUser();
        user.setUserName("Kevin");
        user.setGender("男");
        user.setAge(25);
        return user;
    }
}
